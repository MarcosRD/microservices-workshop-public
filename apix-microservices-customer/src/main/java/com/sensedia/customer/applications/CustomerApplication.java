package com.sensedia.customer.applications;

import com.sensedia.commons.errors.exceptions.BusinessException;
import com.sensedia.commons.errors.exceptions.NotFoundException;
import com.sensedia.customer.adapters.dtos.CustomerDto;
import com.sensedia.customer.adapters.dtos.QualificationsDtoResponse;
import com.sensedia.customer.adapters.http.QualificationsClient;
import com.sensedia.customer.adapters.mappers.CustomerMapper;
import com.sensedia.customer.domains.Customer;
import com.sensedia.customer.ports.AmqpPort;
import com.sensedia.customer.ports.ApplicationPort;
import com.sensedia.customer.ports.RepositoryPort;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Objects;

import static com.sensedia.customer.adapters.dtos.enums.StatusQualificationsEnum.APPROVED;

@Service
@Slf4j
@Transactional
public class CustomerApplication implements ApplicationPort {

    private final AmqpPort amqpPort;
    private final RepositoryPort repository;
    private final QualificationsClient qualificationsClient;
    private final CustomerMapper customerMapper;

    @Autowired
    public CustomerApplication(AmqpPort amqpPort, RepositoryPort repository,
                               QualificationsClient qualificationsClient,
                               CustomerMapper customerMapper) {
        this.amqpPort = amqpPort;
        this.repository = repository;
        this.qualificationsClient = qualificationsClient;
        this.customerMapper = customerMapper;
    }

    @Override
    public Customer create(@Valid @NotNull Customer customer) {
        log.info("Creating customer with name: " + customer.getFirstName());
        if (this.isQualificationCustomerApproved(customer)) {
            log.info("Customer approved: " + customer.getFirstName());

            repository.save(customer);
            amqpPort.publishCustomerCreation(customer);

            return customer;
        }

        log.info("Customer denied: " + customer.getFirstName());
        throw new BusinessException("Customer can't create account.", "Business", "Customer");
    }

    private boolean isQualificationCustomerApproved(Customer customer) {
        // Mapeando nossa entidade para o Dto
        CustomerDto customerDto = this.customerMapper.toCustomerDto(customer);

        // Estamos indo para o adapter para fazer a qualificação do customer
        ResponseEntity<QualificationsDtoResponse> response = this.qualificationsClient.qualify(customerDto);

        if (response.getStatusCode().is2xxSuccessful() && response.hasBody()) {
            return APPROVED.getStatus().equalsIgnoreCase(Objects.requireNonNull(response.getBody()).getStatus());
        }

        return false;
    }

    @Override
    public Customer findById(@NotNull String id) {
        return repository
                .findById(id)
                .orElseThrow(() -> new NotFoundException("Customer not found"));
    }
}
